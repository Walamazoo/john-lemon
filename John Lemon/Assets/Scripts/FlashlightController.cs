﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashlightController : MonoBehaviour
{

    public GameObject slash;
    public float slashDuration = 0.25f;

    float m_Timer = 0.0f;

    void Start()
    {
        slash.SetActive(false);
    }

    void Update()
    {
        if(Input.GetKeyDown("space"))
        {
            slash.SetActive(true);
        }
        m_Timer += Time.deltaTime;
        if(m_Timer > slashDuration)
        {
            m_Timer = m_Timer - slashDuration;
            slash.SetActive(false);
        }
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Ghost"))
        {
           other.gameObject.SetActive(false); 
        }
    }
}
