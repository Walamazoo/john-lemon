﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

//Code for timer created with help from https://gamedevbeginner.com/how-to-make-countdown-timer-in-unity-minutes-seconds/

public class GameEnding : MonoBehaviour
{
    public float fadeDuration = 1f;
    public float displayImageDuration = 1f;
    public float timeRemaining = 610;
    public bool timerIsRunning = false; 
    public TextMeshProUGUI timerText;
    public GameObject introText;
    public GameObject player;
    public CanvasGroup exitBackgroundImageCanvasGroup;
    public AudioSource exitAudio;
    public CanvasGroup caughtBackgroundImageCanvasGroup;
    public AudioSource caughtAudio;

    bool m_IsPlayerAtExit;
    bool m_IsPlayerCaught;
    float m_Timer;
    bool m_HasAudioPlayed;
    float minutes;
    float seconds;

    void Start()
    {
        player.SetActive(false);
        minutes = Mathf.FloorToInt(timeRemaining / 60); 
        seconds = Mathf.FloorToInt(timeRemaining % 60);
        timerText.text = "Time: " + string.Format("{0:00}:{1:00}", minutes, seconds);
        timerIsRunning = true;
    }

    void OnTriggerEnter (Collider other)
    {
        if(other.gameObject == player)
        {
            timerIsRunning = false;
            m_IsPlayerAtExit = true;
        }
    }

    public void CaughtPlayer()
    {
        timerIsRunning = false;
        m_IsPlayerCaught = true;
    }

    void FixedUpdate()
    {
        if( timeRemaining <= 600)
        {
            introText.SetActive(false);
            player.SetActive(true);
        }

         if(timerIsRunning){
            if(timeRemaining > 0){
                timeRemaining -= Time.deltaTime;
                minutes = Mathf.FloorToInt(timeRemaining / 60); 
                seconds = Mathf.FloorToInt(timeRemaining % 60);
                timerText.text = "Time: " + string.Format("{0:00}:{1:00}", minutes, seconds);
            }
            else{
                timeRemaining = 0;
                minutes = Mathf.FloorToInt(timeRemaining / 60); 
                seconds = Mathf.FloorToInt(timeRemaining % 60);
                timerText.text = "Time: " + string.Format("{0:00}:{1:00}", minutes, seconds);
                timerIsRunning = false;
                m_IsPlayerCaught = true;
             }
        }

        if(m_IsPlayerAtExit)
        {
            EndLevel (exitBackgroundImageCanvasGroup, false, exitAudio);
        }
        else if(m_IsPlayerCaught)
        {
            EndLevel (caughtBackgroundImageCanvasGroup, true, caughtAudio);
        }
    }

    void EndLevel (CanvasGroup imageCanvasGroup, bool doRestart, AudioSource audioSource)
    {
        if(!m_HasAudioPlayed)
        {
            audioSource.Play();
            m_HasAudioPlayed = true;
        }
        m_Timer += Time.deltaTime;
        imageCanvasGroup.alpha = m_Timer / fadeDuration + 1;
        if(m_Timer > fadeDuration + displayImageDuration)
        {
            if(doRestart)
            {
                SceneManager.LoadScene(1);
            }
            else
            {
                Application.Quit();
            }
        }
    }
}
